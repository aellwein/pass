# pass 🔐 - Password Generator / Analyzer

CLI tool for generating and analyzing passwords.

## Generator
* [passwords](https://github.com/magiclen/passwords)

## Analyzer
* [passwords](https://github.com/magiclen/passwords)
* [zxcvbn](https://github.com/shssoichiro/zxcvbn-rs)

## Example Usage

### Generate:

```
pass

Password                       passwords score   zxcvbn score
=============================================================
[.EX>kH! 1QQC_[ys<A~       100.00 (invincible)       4 (best)
jU W\D-5Ek%Hn,-jLH2K       100.00 (invincible)       4 (best)
GGZyU08ymD[SB? Ws<U8       100.00 (invincible)       4 (best)
#s-FU| -B1F'L>-SVG?h       100.00 (invincible)       4 (best)
tMupbe5hAmj ?`h.Br<w       100.00 (invincible)       4 (best)
Fg I! s6jH4q~ALj?4b2       100.00 (invincible)       4 (best)
V--:zu )4IO N+P@js4'       100.00 (invincible)       4 (best)
r'qf7 afQ|> 9,P:*XQ{       100.00 (invincible)       4 (best)
8C-9gZ}qlB~j 1=~gL55       100.00 (invincible)       4 (best)
RM v\af#O\!AI(.YU9]%       100.00 (invincible)       4 (best)
```

### Analyze

```
pass a blabla123

Password            passwords score   zxcvbn score
==================================================
blabla123         58.67 (very weak)  1 (very weak)
```

### Options

```
Usage: pass [OPTIONS] [length] [COMMAND]

Commands:
  analyze  Analyzes given password and returs a score for it [aliases: a]
  help     Print this message or the help of the given subcommand(s)

Arguments:
  [length]  password length [default: 20]

Options:
  -n            don't use numbers
  -u            don't use uppercase letters
  -l            don't use lowecase letters
  -s            don't use symbols
  -S            don't use spaces
  -e            exclude similar looking characters, like 'l' and '1' etc.
  -a            don't analyze generated passwords
  -c <num>      amount of passwords to generate [default: 10]
  -h, --help    Print help
```