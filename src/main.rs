use clap::{Arg, ArgAction, ArgMatches, Command};
use passwords::{analyzer::analyze, scorer::score, PasswordGenerator};
use std::{fmt::Display, process::exit};
use zxcvbn::Score;

enum PasswordsScore {
    VeryDangerous(f64),
    Dangerous(f64),
    VeryWeak(f64),
    Weak(f64),
    Good(f64),
    Strong(f64),
    VeryStrong(f64),
    Invincible(f64),
}

enum ZxcvbnScore {
    Dangerous = 0,
    VeryWeak = 1,
    Weak = 2,
    Good = 3,
    Best = 4,
}

struct GeneratorContext {
    length: usize,
    numbers: bool,
    lowercase_letters: bool,
    uppercase_letters: bool,
    symbols: bool,
    spaces: bool,
    exclude_similar_characters: bool,
    analyze: bool,
}

fn cli() -> ArgMatches {
    Command::new("pass")
        .subcommand(
            Command::new("analyze")
                .about("Analyzes given password and returs a score for it")
                .arg(
                    Arg::new("password")
                        .help("Password to be analyzed")
                        .required(true),
                )
                .visible_aliases(["a"]),
        )
        .arg(
            Arg::new("length")
                .help("password length")
                .default_value("20")
                .required(false),
        )
        .arg(
            Arg::new("numbers")
                .action(ArgAction::SetFalse)
                .short('n')
                .required(false)
                .help("don't use numbers"),
        )
        .arg(
            Arg::new("uppercase")
                .action(ArgAction::SetFalse)
                .short('u')
                .required(false)
                .help("don't use uppercase letters"),
        )
        .arg(
            Arg::new("lowercase")
                .action(ArgAction::SetFalse)
                .short('l')
                .required(false)
                .help("don't use lowecase letters"),
        )
        .arg(
            Arg::new("symbols")
                .action(ArgAction::SetFalse)
                .short('s')
                .required(false)
                .help("don't use symbols"),
        )
        .arg(
            Arg::new("spaces")
                .action(ArgAction::SetFalse)
                .short('S')
                .required(false)
                .help("don't use spaces"),
        )
        .arg(
            Arg::new("exclude_similar")
                .action(ArgAction::SetTrue)
                .short('e')
                .required(false)
                .help("exclude similar looking characters, like 'l' and '1' etc."),
        )
        .arg(
            Arg::new("no_analyze")
                .action(ArgAction::SetFalse)
                .short('a')
                .required(false)
                .help("don't analyze generated passwords"),
        )
        .arg(
            Arg::new("num")
                .short('c')
                .default_value("10")
                .required(false)
                .help("amount of passwords to generate"),
        )
        .get_matches()
}

impl From<f64> for PasswordsScore {
    fn from(value: f64) -> Self {
        let i = value.round() as u64;
        match i {
            0..=19 => PasswordsScore::VeryDangerous(value),
            20..=39 => PasswordsScore::Dangerous(value),
            40..=59 => PasswordsScore::VeryWeak(value),
            60..=79 => PasswordsScore::Weak(value),
            80..=89 => PasswordsScore::Good(value),
            90..=94 => PasswordsScore::Strong(value),
            95..=98 => PasswordsScore::VeryStrong(value),
            99..=100 => PasswordsScore::Invincible(value),
            _ => panic!("invalid passwords score: {}", i),
        }
    }
}
impl Display for PasswordsScore {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::VeryDangerous(x) => f.pad(format!("{:6.2} (very dangerous)", *x).as_str()),
            Self::Dangerous(x) => f.pad(format!("{:6.2} (dangerous)", *x).as_str()),
            Self::VeryWeak(x) => f.pad(format!("{:6.2} (very weak)", *x).as_str()),
            Self::Weak(x) => f.pad(format!("{:6.2} (weak)", *x).as_str()),
            Self::Good(x) => f.pad(format!("{:6.2} (good)", *x).as_str()),
            Self::Strong(x) => f.pad(format!("{:6.2} (strong)", *x).as_str()),
            Self::VeryStrong(x) => f.pad(format!("{:6.2} (very strong)", *x).as_str()),
            Self::Invincible(x) => f.pad(format!("{:6.2} (invincible)", *x).as_str()),
        }
    }
}

impl Display for ZxcvbnScore {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Dangerous => f.pad("0 (dangerous)"),
            Self::VeryWeak => f.pad("1 (very weak)"),
            Self::Weak => f.pad("2 (weak)"),
            Self::Good => f.pad("3 (good)"),
            Self::Best => f.pad("4 (best)"),
        }
    }
}

impl From<Score> for ZxcvbnScore {
    fn from(value: Score) -> Self {
        match value {
            Score::Zero => Self::Dangerous,
            Score::One => Self::VeryWeak,
            Score::Two => Self::Weak,
            Score::Three => Self::Good,
            Score::Four => Self::Best,
            _ => panic!("invalid zxcvbn score: {:?}", value),
        }
    }
}

fn get_score(pass: &str) -> (PasswordsScore, ZxcvbnScore) {
    let s1 = score(&analyze(pass));
    let s2 = zxcvbn::zxcvbn(pass, &[]).score();
    (s1.into(), s2.into())
}

fn print_header(analyze: bool, length: usize) {
    let l = length.max("Password".len());
    if analyze {
        println!(
            "{:l$} {:>25} {:>14}",
            "Password", "passwords score", "zxcvbn score",
        );
        println!("{}", "=".repeat(l + 41).as_str());
    }
}

fn analyze_pw(passwd: &str) {
    let (score1, score2) = get_score(passwd);
    print_header(true, passwd.len());
    println!(
        "{:l$} {:>25} {:>14}",
        passwd,
        score1,
        score2,
        l = passwd.len().max("Password".len())
    );
}

fn generate(ctx: GeneratorContext, count: usize) {
    let pg = PasswordGenerator {
        length: ctx.length,
        numbers: ctx.numbers,
        lowercase_letters: ctx.lowercase_letters,
        uppercase_letters: ctx.uppercase_letters,
        symbols: ctx.symbols,
        spaces: ctx.spaces,
        exclude_similar_characters: ctx.exclude_similar_characters,
        strict: true,
    };
    let res = pg.generate(count);
    if let Ok(res) = res {
        print_header(ctx.analyze, ctx.length);
        res.iter().for_each(|passw| {
            if ctx.analyze {
                let (score1, score2) = get_score(passw.as_str());
                println!(
                    "{:l$} {:>25} {:>14}",
                    passw,
                    score1,
                    score2,
                    l = ctx.length.max("Password".len())
                );
            } else {
                println!("{}", passw);
            }
        });
    } else {
        println!("{}", res.unwrap_err());
        exit(1);
    }
}

fn main() {
    let matches = cli();
    match matches.subcommand() {
        Some(("analyze", x)) => analyze_pw(x.get_one::<String>("password").unwrap().as_str()),
        Some((x, _)) => panic!("unhandled command: {}", x),
        None => {
            /* generate */
            let length = str::parse(matches.get_one::<String>("length").unwrap().as_str())
                .expect("invalid password length");
            let numbers = matches.get_one::<bool>("numbers").unwrap();
            let lowercase = matches.get_one::<bool>("lowercase").unwrap();
            let uppercase = matches.get_one::<bool>("uppercase").unwrap();
            let symbols = matches.get_one::<bool>("symbols").unwrap();
            let spaces = matches.get_one::<bool>("spaces").unwrap();
            let exclude_similar = matches.get_one::<bool>("exclude_similar").unwrap();
            let analyze = matches.get_one::<bool>("no_analyze").unwrap();
            let count = str::parse(matches.get_one::<String>("num").unwrap().as_str())
                .expect("Invalid amount of passwords");
            let ctx = GeneratorContext {
                length,
                numbers: *numbers,
                lowercase_letters: *lowercase,
                uppercase_letters: *uppercase,
                symbols: *symbols,
                spaces: *spaces,
                exclude_similar_characters: *exclude_similar,
                analyze: *analyze,
            };
            generate(ctx, count);
        }
    }
}
